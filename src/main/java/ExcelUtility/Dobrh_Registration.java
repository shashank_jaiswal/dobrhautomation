package ExcelUtility;

import java.io.FileInputStream;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Dobrh_Registration {

	public static void main(String[] args) throws Exception 
	
	{
		FileInputStream file= new FileInputStream ("/home/mobcoder/Desktop//Registration.ods");
		@SuppressWarnings("resource")
		XSSFWorkbook book=new XSSFWorkbook(file);
		
		XSSFSheet sheet=book.getSheet("Sheet1");
		int rowNum=sheet.getLastRowNum();
		
		for (int i=1 ; i<=rowNum ; i++)
		{
			XSSFRow recentRow= sheet.getRow(i);
			
		String Email_Id =recentRow.getCell(0).getStringCellValue();
		String User_Name =recentRow.getCell(1).getStringCellValue();
		String First_Name =recentRow.getCell(2).getStringCellValue();
		String Last_Name =recentRow.getCell(3).getStringCellValue();
		String Gender =recentRow.getCell(4).getStringCellValue();
		String Password=recentRow.getCell(5).getStringCellValue();
		String Confirm_Password=recentRow.getCell(6).getStringCellValue();
		String Country=recentRow.getCell(7).getStringCellValue();
		
		
		
		WebDriver driver=new ChromeDriver();
		driver.get("https://stag.dobrh.com/auth/signup");
		
		driver.findElement(By.xpath("//*[@class=\"form-control \"]")).sendKeys(Email_Id);
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow w-100 \"]")).click();
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//form/div[1]/div/div[1]/div")).click();
		
		driver.findElement(By.xpath("//form/div[2]/input")).sendKeys(User_Name);
		driver.findElement(By.xpath("//form/div[4]/div[1]/div/input")).sendKeys(First_Name);
		driver.findElement(By.xpath("//form/div[4]/div[2]/div/input")).sendKeys(Last_Name);
		Select GenderType =new Select (driver.findElement(By.xpath("//form/div[5]/div[1]/div/div[1]/div[2]")));
		GenderType.selectByVisibleText(Gender);
		driver.findElement(By.xpath("//*[@id=\"new_password\"]")).sendKeys(Password);
		driver.findElement(By.xpath("//*[@id=\"new_password2\"]")).sendKeys(Confirm_Password);
		Select countryType =new Select (driver.findElement(By.xpath("//form/div[8]/div[1]/div")));
		countryType.selectByVisibleText(Country);
		driver.findElement(By.xpath("//*[@id=\"__next\"]/div/div/div/div/div/div/form/div[10]/div")).click();
		driver.findElement(By.xpath("//*[@id=\"__next\"]/div/div/div/div/div/div/form/div[11]/div")).click();
		
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow w-100  \"]"));
		
		
		}
				

	}

}
