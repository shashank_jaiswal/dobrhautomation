package JavaLogics;

public class StringReverse {

	public static void main(String[] args) {
		
		String Word = "Programming";
		String RevString = "";
		
		for (int i = Word.length()-1; i>=0; i--) {
			
			RevString += Word.charAt(i);
		}
		
		System.out.println(RevString);


	}

}
